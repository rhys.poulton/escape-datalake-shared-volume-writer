#!/usr/bin/env bash

/bin/bash /etc/profile.d/rucio_init.sh
voms-proxy-init --voms escape --cert /opt/rucio/etc/client.crt --key /opt/rucio/etc/client.key --valid 120:00
/root/miniconda/bin/python /rucio-gwf-shared-vol-writer/rucio-gwf-shared-vol-writer.py /rucio-gwf-shared-vol-writer/RucioDataIn.cfg $@
