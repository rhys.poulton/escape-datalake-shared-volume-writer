FROM projectescape/rucio-client:latest

USER root

RUN wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
RUN bash Miniconda3-latest-Linux-x86_64.sh -b -p ~/miniconda > /dev/null
RUN rm Miniconda3-latest-Linux-x86_64.sh
RUN ~/miniconda/bin/conda install -y -c conda-forge pythonvirgotools

COPY rucio.cfg /opt/rucio/etc/rucio.cfg
COPY rucio-gwf-shared-vol-writer.py /rucio-gwf-shared-vol-writer/
COPY RucioDataIn.cfg /rucio-gwf-shared-vol-writer/
COPY runscript.sh .

VOLUME /datain


ENTRYPOINT [ "./runscript.sh" ]