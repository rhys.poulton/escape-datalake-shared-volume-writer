#!/usr/bin/env python3
import time
import logging
import virgotools as vt
import PyFd as fd
import sys
import numpy as np
import os
from subprocess import Popen, PIPE
from rucio.client import Client
import configargparse
import fileinput

logger = logging.getLogger(__name__)

vt.log_to_console()

parser = configargparse.ArgumentParser(prog="rucio-gwf-shared-vol-writer")
parser.add_argument(
    '-s',
    '--scope',
    type=str,
    required=True,
    help='Rucio scope where the dataset is located',
)
parser.add_argument(
    '-d',
    '--dataset',
    type=str,
    required=True,
    help='Dataset where the file are located',
)
parser.add_argument(
    '-n',
    '--numfiles',
    type=int,
    required=True,
    help='Number of files in the rotating buffer',
)

args = parser.parse_args(sys.argv[2:])

cfg_path = os.path.abspath(sys.argv[1])  # do this first, FdIO changes working dir

# Update the FdIOServer config to use the provided number of files
with open("RucioDataIn.cfg.in", "r") as fin:
    with open(cfg_path, "w") as fout:
        out = fin.read().replace("NUM_FILES", str(args.numfiles))
        fout.write(out)

#Start the fdio server
fdio = vt.FdIO(sys.argv[:2])

indir = vt.parse_key(cfg_path, 'FDIN_DIR')[0][0]
#List all the files in the dataset
filelist = [ifile["name"] for ifile in Client().list_files(args.scope,args.dataset)]
num_files = len(filelist)

if(num_files==0):
    logger.error("ERROR: There are no files present in {scope}:{dataset}")
    raise SystemExit()

#Get the first filename
filename = filelist[0]

#Extract information from the filename
_,_,start_gps,_ = os.path.splitext(filename)[0].split("-")
start_gps = int(start_gps)

#Extract the frame lengths
frame_lens = np.zeros(num_files,dtype=int)
for i,ifile in enumerate(filelist):
    _,_,_,frame_len = os.path.splitext(ifile)[0].split("-")
    frame_lens[i] = int(frame_len)

#Can find the end gps:
end_gps = start_gps  + np.sum(frame_lens)

logger.info(f"There are {num_files} in {scope}:{dataset} with gps range: {start_gps}-{end_gps}")

#Mark the state as configured
fd.CfgReachState(fd.CfgServerConfigured)

#Download the fist file
logger.info("Downloading the {filename} file from rucio")
p = Popen(["rucio","--verbose","download",f"{scope}:{filename}","--no-subdir",'--dir',f"{indir}"],stdout=PIPE,stderr=PIPE)

#Get the output
output, error = p.communicate()

if p.returncode!=0:
    logger.error(f"ERROR: The downloading of {filename} from the datalake failed, exiting")
    print("rucio download failed %d %s" % (p.returncode,error.decode() )) 
    raise SystemExit()

logger.info("Successfully downloaded the first file from Rucio")

#Mark the state as active
fd.CfgReachState(fd.CfgServerActive)

#Mark the state as golden
fd.CfgReachState(fd.CfgServerGolden)

#
next_start_gps = start_gps
ifile = 1
igps = start_gps
prev_filename=""

while not fd.CfgFinished() and igps+1<end_gps:

    #Get the next frame
    in_frame = fdio.get_frame()
    igps = in_frame.gps

    #Check if we need to download the next file.
    if igps>next_start_gps and num_files>ifile:
        next_start_gps += frame_lens[ifile]
        next_filename = filelist[ifile]

        logger.info(f"Downloading {next_filename} from rucio")
        p = Popen(["rucio","--verbose","download",f"{scope}:{next_filename}","--no-subdir",'--dir',f"{indir}"],stdout=PIPE,stderr=PIPE)

        #Delete the previous file
        if prev_filename!="":
            prev_filename_path = os.path.join(indir,prev_filename)
            logger.info(f"Removing {prev_filename_path}")
            os.remove(prev_filename_path)

        prev_filename = filename
        filename = next_filename
        ifile+=1

    #Check if the downloading process failed
    if p.poll() is not None:
        if p.returncode!=0:
            logger.error(f"ERROR: The downloading of {filename} from the datalake failed, exiting")
            print("rucio download failed %d %s" % (p.returncode,p.stderr.read().decode() )) 
            raise SystemExit()

    if(fdio._fdio.contents.userInfo==None):
        fd.CfgMsgAddUserInfo("GPS: %i" %(igps))
        fd.CfgMsgSendWithTimeout(0.001)  

    #Put the frame
    fdio.put_frame(in_frame)