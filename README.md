# Dockerfile to extract Gravitational Wave data from the ESCAPE datalake

This is a container to extract Gravitational Wave (GW) data from the datalake using Rucio and feed 1 second GW frames to the GW pipelines.

To run this container it first needs to be built:
```
 (sudo) docker build  -t rucio-gwf-shared-vol-writer-public .
```
Then it can be run using the following command:
 
```
(sudo) docker run -e RUCIO_ACCOUNT=<rucio account name> -v /path/to/client.crt:/opt/rucio/etc/client.crt -v /path/to/client.key:/opt/rucio/etc/client.key -v
/path/to/output/frames:/dataout rucio-gwf-shared-vol-writer --scope <RUCIO scope> --dataset <RUCIO dataset name> --numfiles <Number of files in the rotating buffer>
```
files will then be streamed to the `/path/to/output/frames` folder, with `NUM_FILES` of file in the rotating buffer.
